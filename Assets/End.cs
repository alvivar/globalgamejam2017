﻿
using UnityEngine;
using UnityEngine.SceneManagement;
using matnesis.TeaTime;

public class End : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag != "Player")
            return;

        var fade = Camera.main.GetComponent<FadeInOut>();
        fade.fadeSpeed = 3;
        fade.fadeDirection = 1;

        this.tt().Add(4, t =>
        {
            SceneManager.LoadScene(0);
        });
    }
}
