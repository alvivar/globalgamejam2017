﻿
// twitter.com/matnesis
// 2017/01/21 06:26 PM


using UnityEngine;
using matnesis.TeaTime;


public class WaveObject : MonoBehaviour
{
    Renderer render;
    Collider collidr;
    float lastTick;
    PlayerActor player;


    void Awake()
    {
        // Defaults
        render = GetComponent<Renderer>();
        render.enabled = false;

        collidr = GetComponent<Collider>();
        // collidr.enabled = false;


        player = Game.player.GetComponent<PlayerActor>();
    }


    void Start()
    {
        // This hides the object when is out of contact with the wave
        // this.tt().Add(0.1f).Add(t =>
        // {
        //     if (Time.time - lastTick > 0.3f)
        //     {
        //         HideYourself();
        //         t.Wait(2);
        //     }
        // })
        // .Repeat();1
    }


    // Signals that there has been a call to show to avoid hiding.
    public void Tick()
    {
        lastTick = Time.time;
    }


    public void ShowYourself()
    {
        Tick();

        // Activation
        render.enabled = true;
        // collidr.enabled = true;

        float current = 0;
        this.tt("@dissolve").Reset().Add(() =>
        {
            current = render.material.GetFloat("_DissolveAmmout");
        })
        .Add(t =>
        {
            // Let's kill the player if overlaps
            if (collidr) player.DieIfOverlaps(collidr.bounds);
        })
        .Loop(0.6f, t =>
        {
            float dissolve = Mathf.Lerp(current, 0, t.t);
            render.material.SetFloat("_DissolveAmmout", dissolve);
        })
        .Add(t =>
        {
            gameObject.layer = LayerMask.NameToLayer("Ground");
        });

    }


    public void HideYourself()
    {
        float current = 0;
        this.tt("@dissolve").Reset().Wait(() => Time.time - lastTick > 0.1f)
        .Add(() =>
        {
            current = render.material.GetFloat("_DissolveAmmout");
            gameObject.layer = LayerMask.NameToLayer("Ghost");
        })
        .Loop(0.3f, t =>
        {
            float dissolve = Mathf.Lerp(current, 1, t.t);
            render.material.SetFloat("_DissolveAmmout", dissolve);
        })
        .Add(t =>
        {
            render.enabled = false;
        });
    }
}
