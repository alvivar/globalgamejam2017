﻿
// @matnesis
// 2017/01/21 10:58 AM


using UnityEngine;
using UnityEngine.SceneManagement;
using matnesis.TeaTime;

public class PlayerActor : MonoBehaviour
{
    [Header("Config")]
    public Transform rotateWithDir;
    public Vector3 rotateWithOffset;
    public Animator animator;
    public Collider collidr;

    [Header("Info")]
    public bool isDead;
    public Vector3 bornPos;

    Motion3D motion;
    Rigidbody rbody;


    public static float FindDegree(int x, int y)
    {
        float value = (float)((Mathf.Atan2(x, y) / Mathf.PI) * 180f);
        if (value < 0) value += 360f;

        return value;
    }


    void Start()
    {
        motion = GetComponent<Motion3D>();
        rbody = GetComponent<Rigidbody>();

        bornPos = transform.position;
    }


    void Update()
    {
        if (transform.position.y != bornPos.y)
        {
            this.tt("@yFixer").Add(t =>
            {
                if (isDead) return;

                var pos = transform.position;
                pos.y = bornPos.y;
                transform.position = pos;
            })
            .Add(1)
            .Immutable();
        }


        // Nothing can be done
        if (isDead)
        {
            animator.SetTrigger("die");
            return;
        }


        // Rotation by direction
        if (rotateWithDir)
            rotateWithDir.forward = Vector3.Lerp(rotateWithDir.forward, motion.direction * -1, Time.deltaTime * 10);


        // Walk or Idle when Walk or Idle :D
        if (motion.direction != Vector3.zero) animator.SetTrigger("walk");
        else animator.SetTrigger("idle");
    }


    // Kills the player, wats.
    public void Die(Vector3 impactFromPos, bool dieWithPhysics = true)
    {
        isDead = true;

        motion.update = false;
        motion.direction = Vector3.zero;
        rbody.velocity = Vector3.zero;

        if (dieWithPhysics)
        {
            rbody.useGravity = true;
            rbody.freezeRotation = false;

            impactFromPos.y = 0;
            rbody.AddExplosionForce(1000, impactFromPos, 3);
        }
        else
        {
            rotateWithDir.gameObject.SetActive(false);
        }


        // Reload
        this.tt().Add(3, t =>
        {
            SceneManager.LoadScene(0);
        });
    }


    // Kills the players if overlappind certain collider bounds!
    public void DieIfOverlaps(Bounds otherBounds)
    {
        var pos = transform.position;
        pos.y = 0;

        if (collidr.bounds.Intersects(otherBounds)) Die(pos, false);
    }
}
