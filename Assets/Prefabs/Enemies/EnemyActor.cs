﻿
// twitter.com/matnesis
// 2017/01/22 01:39 PM


using UnityEngine;
using matnesis.TeaTime;

public class EnemyActor : MonoBehaviour
{
    public Transform rotateWithDir;
    public Animator animator;

    [Header("Auto reference")]
    public Transform target;
    public Motion3D motion;

    PlayerActor player;


    void Start()
    {
        if (!target) target = Game.player;
        if (!motion) motion = GetComponent<Motion3D>();
        if (!animator) animator = GetComponent<Animator>();

        player = Game.player.GetComponent<PlayerActor>();


        // @
        // Follow the player!
        var dir = Vector3.zero;
        this.tt().Add(() => Random.Range(0.1f, 0.4f), t =>
        {
            if (!player.isDead) dir = (target.position - transform.position).normalized;
            else dir = Vector3.zero;

            motion.direction = dir;
        })
        .Repeat();


        // @
        // Look at the target direction!
        this.tt().Loop(t =>
        {
            if (player.isDead) return;

            rotateWithDir.forward = Vector3.Lerp(rotateWithDir.forward, motion.direction * -1, Time.deltaTime * 10);
        });


        // @
        // Animations!
        this.tt().Loop(t =>
        {
            if (player.isDead)
            {
                animator.SetTrigger("idle");
                return;
            }

            if (motion.direction != Vector3.zero) animator.SetTrigger("walk");
            else animator.SetTrigger("idle");
        });
    }


    void OnCollisionEnter(Collision other)
    {
        var tryPlayer = other.transform.GetComponentInParent<PlayerActor>();
        if (tryPlayer)
        {
            animator.SetTrigger("attack");

            this.tt("@OnCollisionEnterAttack").Reset().Add(0.15f, t =>
            {
                tryPlayer.Die(transform.position);
            });
        }
    }
}
