﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using matnesis.TeaTime;
using matnesis.Liteprint;


public class EmitterActor : MonoBehaviour
{
    [Header("Config")]
    public Transform prefabToEmit;

    PlayerActor player;


    void Start()
    {
        player = Game.player.GetComponent<PlayerActor>();


        // @
        //
        this.tt().Add(1, t =>
        {
            var playerPos = player.transform.position;
            playerPos.y = 0;

            var currentPos = transform.position;
            currentPos.y = 0;

            Transform wave = prefabToEmit.liteInstantiate(currentPos, Quaternion.identity);

            Vector3 targetDir = (playerPos - currentPos).normalized;
            targetDir.y = 0;

            float angle = Vector3.Angle(targetDir, transform.forward);
            wave.GetComponent<EmitterVitalActor>().body.localEulerAngles = new Vector3(
                -90,
                angle,
                0
            );

            wave.GetComponent<Motion3D>().direction = targetDir;
        })
        .Add(9).Repeat();

    }
}
