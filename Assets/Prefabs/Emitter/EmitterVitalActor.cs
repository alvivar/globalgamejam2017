﻿
// twitter.com/matnesis
// 2017/01/21 09:19 PM


using UnityEngine;


public class EmitterVitalActor : MonoBehaviour
{
    public Transform body;
    public Renderer groundRenderer;


    void Update()
    {
        // This is a #chanchado I do, because the precise direction doesn't work
        // right.
        if (transform.position.y != 0)
        {
            var pos = transform.position;
            pos.y = 0;
            transform.position = pos;
        }

        // Modifying the offset from the secondary map
        groundRenderer.material.SetTextureOffset("_DetailAlbedoMap", new Vector2(Time.time * -0.9f, 0));
    }


    void OnTriggerEnter(Collider other)
    {
        var wave = other.GetComponent<WaveObject>();
        if (wave)
        {
            wave.ShowYourself();
        }
    }


    // This only works if the collider is one piece, not several
    void OnTriggerExit(Collider other)
    {
        var wave = other.GetComponent<WaveObject>();
        if (wave)
        {
            wave.HideYourself();
        }
    }


    // In last case, let's use this
    // void OnTriggerStay(Collider other)
    // {
    //     var wave = other.GetComponent<WaveObject>();
    //     if (wave)
    //     {
    //         wave.Tick();
    //     }
    // }
}
