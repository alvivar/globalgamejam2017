###**Hasten**
**Unity GameDev Toolkit**<br /><br />

This is the code I use in all [my games](http://matnesis.itch.io/). It's
a collection of scripts, extensions, patterns and custom tools for Unity.

I have managed to create all my stuff with versions of this, and I'm
constantly improving it with every new creation. Maybe you can find something
useful, take a look.

<br />—<br />
[Andrés Villalobos](http://twitter.com/matnesis)
