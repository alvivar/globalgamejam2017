﻿public class Layer
{
    public const string Player = "Player";
    public const string PlayerDetection = "PlayerDetection";
    public const string PlayerDamage = "PlayerDamage";
    public const string NPC = "NPC";

    public const string Enemy = "Enemy";
    public const string Ghost = "Ghost";

    public const string Teleportable = "Teleportable";
    public const string Walkable = "Walkable";

    public const string Ground = "Ground";
    public const string DeadGround = "DeadGround";

    public const string Conversation = "Conversation";
    public const string House = "House";
    public const string Bushes = "Bushes";
    public const string Fruits = "Fruits";

    public const string Checkpoint = "Checkpoint";
    public const string Score = "Score";
}
