﻿
// Custom Rigidbody2D Motion a la CharacterController.

// Andrés Villalobos ~ andresalvivar@gmail.com ~ twitter.com/matnesis
// 2015/08/11 10:13 PM


using UnityEngine;
using matnesis.TeaTime;


[RequireComponent(typeof(Rigidbody))]
// [RequireComponent(typeof(CapsuleCollider))]
public class Motion3D : MonoBehaviour
{
    public bool update = true;


    // ^
    // Rigidbody2D.velocity connection
    public Vector3 velocity
    {
        get { return rbody.velocity; }
        set { rbody.velocity = value; }
    }


    [Header("Config")]
    [Range(0, 10)]
    public float attack;
    [Range(0, 10)]
    public float decay;
    public float limit;
    public float limitOverride;

    [Header("Constants")]
    public Vector3 direction;
    public float magnitude;

    [Header("Forces")]
    public Vector3 gravity;
    public Vector3 gravityOverride;
    public Vector3 force;
    public float forceLimit;

    private Rigidbody rbody;
    private Collider collidr;
    private Vector3 movement;

    // tt
    private TeaTime wallCollisionDetection;


    void Start()
    {
        // Required
        rbody = GetComponent<Rigidbody>();
        collidr = GetComponent<Collider>();
    }


    void Update()
    {
        if (!update) return;


        // Direction * magnitude
        movement = transform.TransformDirection(direction.normalized);
        movement *= magnitude;


        // Gravity + Override
        if (gravityOverride != Vector3.zero)
            movement += gravityOverride;
        else
            movement += gravity;


        // Limit + Override
        if (limitOverride != 0)
            movement = Vector3.ClampMagnitude(movement, limitOverride);
        else
            movement = Vector3.ClampMagnitude(movement, limit);


        // Unlimited force
        if (forceLimit != 0)
            movement += Vector3.ClampMagnitude(force, forceLimit);
        else
            movement += force;


        // Velocity
        rbody.velocity = Vector3.Lerp(rbody.velocity, movement, Time.deltaTime * attack);
        rbody.velocity = Vector3.Lerp(rbody.velocity, Vector3.zero, Time.deltaTime * decay);
    }


    public void Reset()
    {
        movement = rbody.velocity = Vector3.zero;
    }
}
