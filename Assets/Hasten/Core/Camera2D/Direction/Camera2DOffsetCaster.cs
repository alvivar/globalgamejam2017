﻿
// This component updates the Camera2D offset with the closest
// Camera2DOffsetData.

// @matnesis
// 2016/10/09 06:14 PM


using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using matnesis.TeaTime;

public class Camera2DOffsetCaster : MonoBehaviour
{
    public float tolerance = 16; // Units of distance to search between movement
    public float changeDuration = 2;
    public LayerMask layerThatBlocks; // The layers that blocks detections

    [Header("Info")]
    public Vector3 lastPosition;
    public List<Camera2DOffsetData> offsets;

    private Camera2D cam;
    private static MonoBehaviour mono;


    void Start()
    {
        cam = Game.camera2D;
        if (mono == null) mono = this;


        // @
        // This sequence updates the Camera2D offset with the closest
        // Camera2DOffsetData.
        {
            Camera2DOffsetData latest = null;
            lastPosition = Vector3.zero;

            this.tt().Add(0.50f, (ttHandler t) =>
            {
                // Do they exists?
                if (Camera2DOffsetData.all == null)
                {
                    InterpolateCamera2DOffsetOverride(Vector3.zero);
                    return;
                }

                // Are we in a new position?
                var snapPos = Snap(transform.position, tolerance);
                if (snapPos != lastPosition)
                {
                    lastPosition = snapPos;

                    // Sort them
                    offsets = Camera2DOffsetData.all
                        .OrderBy(x => (snapPos - x.transform.position).sqrMagnitude)
                        .ToList();

                    // Update if new
                    if (offsets.Count > 0 && offsets[0] != latest)
                    {
                        // Calculations
                        var tPos = transform.position;
                        var oPos = offsets[0].transform.position;
                        var dir = (oPos - tPos).normalized;
                        var distance = Vector3.Distance(oPos, tPos);

                        // Ignore if there is something between us
                        if (Physics2D.Raycast(transform.position, dir, distance, layerThatBlocks))
                            return;

                        // Interpolate
                        latest = offsets[0];
                        InterpolateCamera2DOffsetOverride(offsets[0].offset);
                        Debug.Log("Camera2D :: Offset override at " + offsets[0].offset);
                    }
                }
            })
            .Repeat();
        }
    }


    private void InterpolateCamera2DOffsetOverride(Vector3 newOffset)
    {
        // Interpolation
        Vector3 current = cam.focusOffsetOverride;
        mono.tt("@InterpolateCamera2DOffsetOverride").Reset().Loop(changeDuration, (ttHandler t) =>
        {
            cam.focusOffsetOverride = Vector3.Lerp(
                current,
                newOffset,
                Easef.Smootherstep(t.t)
            );
        });
    }


    public static Vector3 Snap(Vector3 vector, float size)
    {
        return new Vector3(
            Mathf.Round(vector.x / size) * size,
            Mathf.Round(vector.y / size) * size,
            Mathf.Round(vector.z / size) * size
        );
    }
}
