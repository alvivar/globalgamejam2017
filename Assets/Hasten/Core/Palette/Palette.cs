﻿
// Simple color collection.

// @matnesis
// 2015/09/30 09:48 PM


using UnityEngine;

public class Palette : MonoBehaviour
{
	[Header("Gray")]
	public Color grayLite;
	public Color grayStrong;

	[Header("Red")]
	public Color redAlert;
	public Color redSoft;

	[Header("Yellow")]
	public Color yellowBlink;

	[Header("Blue")]
	public Color bluePale;
}
